package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TECHNOLOGY")
public class Technology implements Serializable {

	private static final long serialVersionUID = 3250848318045176439L;
	
	@Id
	@Column(name="TECHNOLOGY_ID")
	@GeneratedValue
	private Long technology_id;
	
	@Column(name="BORDER_COLOR", length=7)
	private String border_color;
	
	@Column(name="BACKGROUND_COLOR", length=7)
	private String background_color;

	@Column(name="NAME")
	private String name;

	public Long getTechnology_id() {
		return technology_id;
	}

	public void setTechnology_id(Long technology_id) {
		this.technology_id = technology_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBorder_color() {
		return border_color;
	}

	public void setBorder_color(String border_color) {
		this.border_color = border_color;
	}

	public String getBackground_color() {
		return background_color;
	}

	public void setBackground_color(String background_color) {
		this.background_color = background_color;
	}
	
	
}
