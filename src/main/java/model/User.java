package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

	private static final long serialVersionUID = -6040439799546703939L;

	@Id
	@Column(name="USER_ID")
	@GeneratedValue
	private Long user_id;
	
	@Column(name="DAS", length=8)
	private String das;

	@Column(name="NAME")
	private String name;
	
	@Column(name="PASSWORD")
	@JsonIgnore
	private String password;
		
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ROLE_ID", nullable = true)
	private Role role;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="TECHNOLOGY_ID", nullable = true)
	private Technology technology;

	public String getDas() {
		return das;
	}

	public void setDas(String das) {
		this.das = das;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public Technology getTechnology() {
		return technology;
	}

	public void setTechnology(Technology technology) {
		this.technology = technology;
	}
}
