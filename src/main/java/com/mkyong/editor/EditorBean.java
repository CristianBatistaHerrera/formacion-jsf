package com.mkyong.editor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@ManagedBean(name = "editor")
public class EditorBean {
	
	private String prueba;
	
	@Resource(name="jdbc/cedeidb")
	private DataSource ds;
	
	@PostConstruct
	public void init() {
		this.prueba = "asdasd";
		try {
			Context ctx = new InitialContext();
			ds = (DataSource)ctx.lookup("java:comp/env/jdbc/cedeidb");
		  } catch (NamingException e) {
			e.printStackTrace();
		  }
		obtainUserData();
	}
	
	private void obtainUserData() {
		Connection con;
		PreparedStatement ps;
		ResultSet result;
		
		try {
			con = ds.getConnection();
			ps = con.prepareStatement("SELECT * FROM prueba");
			result = ps.executeQuery(); 
			while(result.next()) {
				System.out.println(result.getObject("id"));
			}
		} catch (SQLException e) {
			System.out.println("Error accediendo a BD: " + e);
		}

	}

	public String getPrueba() {
		return prueba;
	}

	public void setPrueba(String prueba) {
		this.prueba = prueba;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

}